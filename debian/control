Source: wolframalpha
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Ethan Ward <ethan.ward@mycroft.ai>
Build-Depends: debhelper-compat (= 10),
               dh-python,
               python-all,
               python-jaraco.itertools,
               python-setuptools,
               python-setuptools-scm,
               python-six,
               python-xmltodict,
               python3-all,
               python3-jaraco.itertools,
               python3-setuptools,
               python3-setuptools-scm,
               python3-six,
               python3-xmltodict
Standards-Version: 4.0.0
Homepage: https://pypi.python.org/pypi/wolframalpha
Vcs-Git: https://salsa.debian.org/python-team/packages/wolframalpha.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/wolframalpha

Package: python-wolframalpha
Architecture: all
Depends: ${misc:Depends}, ${python:Depends}
XB-Python-Version: ${python:Versions}
Description: Wolfram|Alpha 2.0 API client (Python 2)
 Python client built against the Wolfram|Alpha v2.0 API. This allows querying
 Wolfram Alpha with any query and then easily parsing the results.
 .
 This package contains the module for Python 2.

Package: python3-wolframalpha
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: Wolfram|Alpha 2.0 API client (Python 3)
 Python client built against the Wolfram|Alpha v2.0 API. This allows querying
 Wolfram Alpha with any query and then easily parsing the results.
 .
 This package contains the module for Python 3.
